﻿using System;
using System.Collections.Generic;
using System.Linq;
using OfficeOpenXml;
using System.Text;
using System.Threading.Tasks;
using ShopifySharp;
using System.IO;
using ShopifySharp.Filters;

namespace ShopCollMgr
{
    class Program
    {
        private static ExcelWorksheet eSheet;
        private static SmartCollectionService cService;
        private static SmartCollection smColl = null;

        private static int FindWorksheet(ExcelPackage infile, string worksheetName)
        {
            var worksheetCounts = infile.Workbook.Worksheets.Count;
            var found = false;
            int i = 1;

            while (!found && i <= worksheetCounts)
            {
                if (infile.Workbook.Worksheets[i].Name.ToUpper() == worksheetName.ToUpper())
                {
                    found = true;
                }
                else
                {
                    i += 1;
                }
            }

            if (!found)
            {
                Console.WriteLine($"Cannot find worksheet {worksheetName}");
                i = 0;
            }
            return i;
        }

        static void Main(string[] args)
        {

            cService = new SmartCollectionService("https://2esc-pilot.myshopify.com", "b1fe0fa4e526b767bd7268df7e04a61c");
            Console.WriteLine("Opening Collection Spreadsheet");
            var startRow = 2; // Adjust these two to get all the rows. 
            var endRow = 303;
            var inpath = @"C:\Mystuff\VSProjects\ShopCollMgr\Collections.xlsx";
            var pfile = new ExcelPackage(new FileInfo(inpath));

            var i = FindWorksheet(pfile, "Sheet1");

            eSheet = pfile.Workbook.Worksheets[i];

            string smCollName = "";
            string pType = "";

            for (var r = startRow; r <= endRow; r++)
            {
                smCollName = eSheet.Cells[r, 1].Text;

                var filter = new SmartCollectionFilter()
                {
                    Title = smCollName
                };

                System.Threading.Thread.Sleep(1000);
                var x = cService.ListAsync(filter).Result.ToList();

                if (x.Count > 0)
                {
                    smColl = x[0];
                }
                else
                {
                    smColl = new SmartCollection()
                    {
                        Title = smCollName,
                        Rules = new List<SmartCollectionRules>()
                    };
                }

                List<SmartCollectionRules> rules = smColl.Rules.ToList();

                int cnt = 2;
                pType = eSheet.Cells[r, cnt].Text;
                do
                {

                    if (!rules.Any(xx => xx.Condition == pType))
                    {
                        rules.Add( new SmartCollectionRules()
                        {
                            Condition = pType,
                            Relation = "equals",
                            Column = "type"
                        }
                        );
                    }

                    cnt = cnt + 1;
                    pType = eSheet.Cells[r, cnt].Text;

                } while (!String.IsNullOrEmpty(pType));

                if (x.Count > 0)
                {

                    smColl = cService.UpdateAsync((long)smColl.Id, new SmartCollection()
                    {
                        Rules = rules
                    }).Result;

                }
                else
                {
                    smColl = cService.CreateAsync(new SmartCollection()
                    {
                        Title = smCollName,
                        Rules = rules,
                        Disjunctive = true
                    }).Result;
                }

            }


            Console.WriteLine("Completed");
            Console.ReadLine();

        }
    }
}
